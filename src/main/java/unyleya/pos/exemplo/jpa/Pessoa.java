package unyleya.pos.exemplo.jpa;

import javax.persistence.*;

@Entity
@Table(name="Pessoa")
public class Pessoa {

	@Id
	@Column(name="id")
    @GeneratedValue
	private Long id;

	@Column(name="nome")
	private String nome;


	@Column(name="idade")
	private Long idade;

    @Column(name="endereco")
    private String endereco;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

    public Long getIdade() {
        return idade;
    }

    public void setIdade(Long idade) {
        this.idade = idade;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
}