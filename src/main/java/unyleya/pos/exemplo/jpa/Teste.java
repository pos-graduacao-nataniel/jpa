package unyleya.pos.exemplo.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Scanner;

public class Teste {



	private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence
			.createEntityManagerFactory("JPAExemplo");

	public static EntityManager getEntityManager() {
		return ENTITY_MANAGER_FACTORY.createEntityManager();
	}


	public static void main(String[] args) {

		EntityManager entityMgr = getEntityManager();
		entityMgr.getTransaction().begin();

		Pessoa pessoa = new Pessoa();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Digite o seu nome: ");
        String nome = scanner.nextLine();
        System.out.println("Você digitou " + nome);

        System.out.println("Digite o sua idade: ");
        Long idade = scanner.nextLong();
        System.out.println("Você digitou " + idade);

        System.out.println("Digite o seu endereço: ");
        String endereco = scanner.next();
        System.out.println("Você digitou " + endereco);

		pessoa.setNome(nome);
		pessoa.setIdade(idade);
		pessoa.setEndereco(endereco);
		entityMgr.persist(pessoa);

		entityMgr.getTransaction().commit();

		entityMgr.clear();
		System.out.println("Registro cadastrado com sucesso!");
	}
}